import java.util.Scanner;
import java.util.ArrayList;

public class TestProduto{

	public static void main(String [] args){

		Scanner s = new Scanner(System.in);
		Scanner l = new Scanner(System.in);
		int q;
		String nomeP;
		String data;
		double preco, total = 0, pf;
		int cod = 0, aux, aux2, qt;
		Produto p;
		InsereProduto ins = new InsereProduto();
		ArrayList<Produto> lista =new ArrayList<Produto>();
		do{
			menu();
			q = s.nextInt();
			switch(q)
			{
				case 1: 
					System.out.println("\nDescrição: ");
					nomeP = l.nextLine();
					System.out.println("Preço: ");
					preco = s.nextDouble();	
					if(preco <= 0)
					{
						System.out.println("Preço inválido, digote novamente!");
						System.out.println("Preço: ");
						preco = s.nextDouble();	
					}
					System.out.println("Data de vencimento: ");
					data = s.next();
					p = new Produto(preco, nomeP, cod, data);
					p.setQtd(0);
					ins.cad(p);
					cod ++;
					break;
					
				case 2: 
					ins.imprime();
					System.out.println("Por favor, informe o cod do produto");
					aux = s.nextInt();		
					
					if(ins.tam() <= aux && ins.tam() != 0)
					{
						System.out.println("O produto não foi cadastrado, informe novamente");
						System.out.println("Por favor, informe o código do produto");
						aux = s.nextInt();
					}
					else if(ins.tam() == 0)
					{
						System.out.println("Sem cadastros, por isso voltará para o menu principal");
						break;
					}
					System.out.println("Por favor, informe a quantidade:  ");
					qt = s.nextInt();		
					ins.mod(aux, qt);
					break;
					
				case 3: 
					ins.imprime();
					break;
					
				case 4: 
					System.out.println("Total de arrecadação: " + total);
					break;
				
				case 5: 
					ins.imprime();
					System.out.println("Por favor, informe o código do produto");
					aux2 = s.nextInt();		
					System.out.println("Por favor, informe a quantidade:  ");
					qt = s.nextInt();
					if(lista.size() < aux2)
					{
						System.out.println("Produto não cadastrado ou em falta");
						break;
					}
					else 
					{
						if(ins.vend(aux2, qt) == 0)
						{
							System.out.println("quantidade inválida, por favor, informe novamente!");
							ins.imprime();
							System.out.println("Por favor, informe o cod do produto");
							aux = s.nextInt();		
							System.out.println("Por favor, informe a quantidade:  ");
							qt = s.nextInt();
						}
						pf = ins.vend(aux2, qt);
						System.out.println("Preço final: " + pf + " R$");
						total += pf;
						break;
					}
					
				case 6: System.exit(0);
					break;
			}
		}while(true);
	}
	
	public static void menu() {
		System.out.printf("\n 1 - Cadastrar Produto\n 2 - Inserir Produto\n 3 - Listar Produtos\n 4 - Listar faturamento\n 5 - Efetuar venda\n 6 - Sair\n");
		System.out.printf("\nEscolha a opção desejada: ");
	}
}