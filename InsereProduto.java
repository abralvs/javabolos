import java.util.ArrayList;

public class InsereProduto
{ 	
	//public int i;
	Produto p;
	ArrayList<Produto> lista =new ArrayList<Produto>();
	
	public void cad(Produto p)
	{
		lista.add(p);
	}
	
	public void imprime()
	{
		System.out.printf("\nLista de produtos:\n");
		for (Produto p : (ArrayList<Produto>) lista) {
			System.out.println("\nCódigo do produto: "+ p.getCod()
			+ "\nDescrição: " + p.getNome() 
			+ "\nPreço: " + p.getPreco() + " R$"
			+ "\nData de vencimento: " + p.getVenc() 
			+ "\nQuantidade: " + p.getQtd());
		}
	}
	
	public void mod(int aux, int qt)
	{
		lista.get(aux).setQtd(qt);
	}
	
	public double vend(int aux, int qt)
	{
		int bef = lista.get(aux).getQtd();
		int aft = bef - qt;
		if(bef < qt)
			return 0;
		else {
			lista.get(aux).setQtd(aft);
			return (qt*lista.get(aux).getPreco());
		}	
	}
	public int tam()
	{
			return lista.size();
	}
	
}