public class Produto 
{
	private double preco;
	private String nome;
	private int codigo;
	private String vencimento;
	private int quantidade;

	
	public Produto(double preco, String nome, int codigo, String vencimento)
	{
		this.nome = nome;
		this.preco = preco;
		this.codigo = codigo;
		this.vencimento = vencimento;
	}
	public void setPreco(double preco)
	{
		this.preco = preco;
	}
	
	public double getPreco()
	{
		return preco;
	}
	
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	public String getNome()
	{
		return nome;
	}
	public void setCod(int codigo)
	{
		this.codigo = codigo;
	}
	
	public int getCod()
	{
		return codigo;
	}
	public void setQtd(int qtd)
	{
		this.quantidade = qtd;
	}
	
	public int getQtd()
	{
		return quantidade;
	}
	
	public void setVenc(String data)
	{
		this.vencimento = data;
	}
	
	public String getVenc()
	{
		return vencimento;
	}
}

